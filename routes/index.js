const express = require('express');
const router = express.Router();

const mongoose = require('mongoose');
const Task = require('../models/task.js');
const Developer = require('../models/developer.js');

router.get('/', function (req, res) {
    res.render('index', {pageTitle: 'Index'})
});

router.get('/newTask', function (req, res) {
    Developer.find()
        .then(developers => {
            res.render("newTask", {
                pageTitle: 'New Task',
                developers: developers
            });
        })
        .catch(err => {
            throw err;
        });
});

router.post('/addTask', function (req, res) {
    let taskName = req.body.taskName;
    let taskAssignee = req.body.taskAssignee;
    let taskDue = req.body.taskDue;
    let taskStatus = req.body.taskStatus;
    let taskDescription = req.body.taskDescription;
    let task = new Task({
        _id: mongoose.Types.ObjectId(),
        name: taskName,
        due: taskDue,
        status: taskStatus,
        description: taskDescription,
        developer: taskAssignee
    });
    task.save()
        .then(result => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/listTasks', function (req, res) {
    Task.find()
        .populate('developer')
        .then(tasks => {
            res.render("listTasks", {
                pageTitle: 'Task List',
                tasks: tasks
            });
        })
        .catch(err => {
            throw err;
        })
});

router.get('/deleteTask', function (req, res) {
    res.render('deleteTask', {pageTitle: 'Delete Task'})
});

router.post('/deleteTask', function (req, res) {
    const taskID = req.body.taskID;
    Task.findByIdAndRemove(taskID)
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.post('/deleteCompletedTasks', function (req, res) {
    const taskID = req.body.taskID;
    Task.deleteMany({'status': 'Complete'})
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.post('/deleteAllTasks', function (req, res) {
    const taskID = req.body.taskID;
    Task.deleteMany()
        .then(() => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/updateTask', function (req, res) {
    res.render('updateTask', {pageTitle: 'Update Task'})
});

router.post('/updateTask', function (req, res) {
    let taskID = req.body.taskID;
    let taskStatus = req.body.taskStatus;
    Task.findById(taskID)
        .then(task => {
            task.status = taskStatus;
            return task.save();
        })
        .then(result => {
            res.redirect('/listTasks');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/newDeveloper', function (req, res) {
    res.render('newDeveloper', {pageTitle: 'Add New Developer'})
});

router.post('/addDeveloper', function (req, res) {
    let developerNameFirst = req.body.name;
    let developerNameLast = req.body.surname;
    let developerLevel = req.body.level;
    let developerState = req.body.state;
    let developerSuburb = req.body.suburb;
    let developerStreet = req.body.street;
    let developerUnit = req.body.unit;
    let developer = new Developer({
        _id: mongoose.Types.ObjectId(),
        name: {
            firstName: developerNameFirst,
            surname: developerNameLast
        },
        level: developerLevel,
        address: {
            state: developerState,
            suburb: developerSuburb,
            street: developerStreet,
            unit: developerUnit
        }
    });
    developer.save()
        .then(result => {
            res.redirect('/listDevelopers');
        })
        .catch(err => {
            throw err;
        });
});

router.get('/listDevelopers', function (req, res) {
    Developer.find()
        .then(developers => {
            res.render("listDevelopers", {
                pageTitle: 'Developer List',
                developers: developers
            });
        })
        .catch(err => {
            throw err;
        })
});

module.exports = router;