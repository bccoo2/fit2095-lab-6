const express = require('express');
const router = require('./routes');
const bodyParser = require('body-parser');
const path = require('path');
const mongoose = require('mongoose');
const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');
mongoose.set('useNewUrlParser', true);

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static(path.join(__dirname, 'public')));
app.use('/css', express.static(path.join(__dirname, 'public/stylesheets/')));

app.use('/', router);
mongoose.connect('mongodb+srv://dbuser:l6eASYPIUEk1dc8J44Gu56ZTH7q@cluster0-7h1s4.azure.mongodb.net/test?retryWrites=true&w=majority')
    .then(result => {
        app.listen(process.env.PORT || 4000, function () {
            console.log('Your node js server is running');
        });
        console.log("Server running at http://localhost:4000");
    })
    .catch(err => {
        throw err;
    });