const mongoose = require('mongoose');

let taskSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    due: {
        type: Date,
        default: Date.now(),
        required: true
    },
    status: {
        type: String,
        validate: {
            validator: function (statusVal) {
                return statusVal === "Complete" || statusVal === "InProgress";
            }
        },
        required: true
    },
    description: {
        type: String,
        required: true
    },
    developer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Developer',
        required: true
    }
});

module.exports = mongoose.model('Task', taskSchema);